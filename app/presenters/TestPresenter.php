<?php

namespace App\Presenters;

use Nette;
use Nette\Forms\Form;
use Nette\Utils\Validators;
use Nette\Utils\Json;


/**
 * Test presenter.
 */
class TestPresenter extends BasePresenter
{

    public function renderDefault()
    {
        $form = new Form();
        $form->setMethod('POST');
        $form->setAction($this->link('submit!'));
        $form->addTextArea('data','Submit your JSON string')
            ->setRequired(true);
        $form->addSubmit('submit','Submit');

        $this->template->form = $form;
    }

    public function handleSubmit() {
        if($this->request->isMethod('POST')) {
            $data = Json::decode($this->request->getPost()['data'],Json::FORCE_ARRAY);
            if (is_array($data) && (!empty($data))) {
                $out = [];
                $i = 0;
                foreach($data as $person) {
                    $out[$i] = [];
                    if(!empty($person['surname'])) {
                        // TODO: define naming convention for validation
                        $out[$i]['name'] = $person['surname'];
                    } else {
                        continue; // At least surname is required
                    }
                    if(!empty($person['name'])) {
                        // TODO: define naming convention for validation
                        $out[$i]['name'] .= ", " . $person['name'];
                    }
                    if((!empty($person['mail']))
                        && (Validators::isEmail($person['mail']))) {
                        $out[$i]['mail'] = $person['mail'];
                    }
                    if((!empty($person['phone']))
                        && (Validators::isNumericInt($person['phone']))
                        && strlen($person['phone']) == 9) {
                        $out[$i]['phone'] = '00420' . $person['phone'];
                    }
                    if(!empty($out[$i])) {
                        $i++;
                    }
                }

                $postData = Json::encode($out);
                $ch = curl_init('http://reciever.brandnew.cz/');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($postData))
                );
                curl_exec($ch);
                $this->flashMessage('Your data has been successefully processed!');
            } else {
                $this->flashMessage('You have submitted incorrect JSON string.','error');
            }
        }
    }

}